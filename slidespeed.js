/*
	slideSpeed plugin by @mikestreety
	https://github.com/mikestreety/slideSpeed.git
*/
(function($){
	$.fn.slideSpeed = function(options) {
		var defaults = {
			speed: 500, //Pixels per second
			slide: 'down'.toLowerCase(), //can be up, down or toggle - made lowercase to matchif statment below
			callback: function() {} // allows dev to get a callback
		};
		options = $.extend(defaults, options);

		return this.each(function() {
			obj = $(this);
			var distance = obj.height();
			var speed = options.speed;
			var time = (Math.round(distance/speed)) * 1000; //make sure the time is an integer to avoid complications

			// this is a cool hack to avoid having to do if(options.slide == "up" || options.slide == ...)
			obj["slide" + options.slide.charAt(0).toUpperCase() + options.slide.slice(1)](time, options.callback);
		});
	};
})(jQuery);
