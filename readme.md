# slideSpeed


slideSpeed allows you to control elements sliding up or down with a set speed rather than a timing. This means taller elements take longer to slide - more representative of their size.

The speed is in **pixels per second** - best bet is to have a play around and see what you prefer.

This plugin is used instead of jQuery's .slideDown(), .slideUp() or .slideToggle()

for example:

	$('div').slideSpeed();

The slideSpeed() plugin can take 3 parameters:

	$('div').slideSpeed({
		speed: 500, //the speed
		slide: 'down', //can be up, down or toggle.
		callback: function(){} // gets fired once the element has done its thing
	});

The values listed in the example above are the defaults for the plugin.

To give you an idea of speed and time, the following results were conducted 

For a div with a height of 600px - I achieved the following results:

<table>
	<tr>
		<th>Speed (pixels per second)</th>
		<th>Time (ms)</th>
	</tr>
	<tr>
		<td>200</td>
		<td>3000</td>
	</tr>
	<tr>
		<td>500</td>
		<td>1200</td>
	</tr>	
	<tr>
		<td>800</td>
		<td>800</td>
	</tr>
</table>


